# SYSE6301: Value Added Cloud Application and Systems

Make sure to review all information found [here](https://gitlab.com/systems-iot/resources-and-tools). This information will be invaluable to you as you complete the assignment. A significant number of details were not able to be provided in the lecture due to time constraints. Those details can be found in the resource videos and write-ups. Need access? Notify your instructor.

### Assignment Context

The following assignment steps you through the necessary fundamentals to creating and deploying a value added cloud infrastructure system. It is highly recommended to review material and watch [https://gitlab.com/systems-iot/api-server-implementation](https://gitlab.com/systems-iot/api-server-implementation). Most of the answers to this homework can be found there.

### Logistical Items

The following assignment is given in steps. It is important to follow the steps and review each step carefully before attempting to complete the next step.

Some of the steps will require you to answer a question (notated with _*Question*_). Your answers to these questions should be provided in a final report write-up you will submit along with a link to a git repository of your working code (they can be provided in a README.md file in the final submitted repository).

## A. Setting up your environment

1. If you have not done so already, register for https://gitlab.com account. Notify your instructor of your username, so you can be added to the resources and tools repository. You should have already completed this step in the previous IoT assignment.

2. Make sure that you have installed Python 3. This should have already been done in the previous assignment. If not, review the steps here: [https://gitlab.com/systems-iot/resources-and-tools/blob/master/python.md](https://gitlab.com/systems-iot/resources-and-tools/blob/master/python.md)

3. If you have not done so already, register for an Iotery account at [https://iotery.io/register](https://iotery.io/register) -- you should already have an account, so simply create a new team for this assignment in step next)

   - Create a new Team called `SYSE6301-VA-[your-gitlab-username]` (option found on on the left menu)

4. Clone the [https://gitlab.com/systems-iot/api-server-implementation](https://gitlab.com/systems-iot/api-server-implementation) repository, then make sure to delete the `.git` folder that is in the cloned folder (it may be hidden!).

5. Open up a terminal (or in Windows, make sure to open up a git shell) and navigate to your cloned folder where you deleted the `.git` folder. Once you are in the folder, run the command `git init`. This will reinitialize your new folder with “track changes” (versioning) using git. Upon each sub-step in the assignment, you may want to create a “branch”, where the branch will be a “snapshot” of your current work. Each step’s work will then be individually accessible.

6. Create a new project called `SYSE6301-VA` in your Gitlab account and give access to the instructor and grader through the Gitlab Dashboard menu (Settings > Members)

7. On the Gitlab dashboard inside your newly created project, look for the blue `clone` button. Clicking on the blue “clone” button, copy the URL provided underneath the “Clone with HTTPS” (it will look like [https://gitlab.com/your-gitlab-username/SYSE6301-VA.git](https://gitlab.com/your-gitlab-username/SYSE6301-VA.git)).
8. In the same terminal you ran the `git init` command in step A.8, run the command: `git remote add origin [URL-you-copied-from-step-1.G]`. This will connect your local git repository to Gitlab.
9. In that same terminal, run `git add .`
10. Then run, `git commit -m “my initial commit”`
11. Then run, `git push origin master`
12. You should now see your files on your Gitlab dashboard. At any point, you can push your work to your Gitlab repository by repeating steps A.10-A.12.
13. _*Question*_: What operating system are you running (Windows, MacOS, or Linux)?

## B: Running your Value Added System (VAS)

1. In your new project folder, create a virtualenv: `virtualenv venv`
2. Activate your virtual environment (MacOS and Linux): `source venv/bin/activate`
   - For Windows, from "Command Prompt" or "Terminal" application run `venv\Scripts\activate`
3. Once activated, install the dependencies (flask, etc): `pip install -r requirements.txt`
4. Start the server: `python app.py` (you can change the port from 5000 to 9998 by specifying `PORT=9998 python app.py`)
5. Verify your server is working by visiting the URL [http://localhost:5000/hello/systems](http://localhost:5000/hello/systems) in a browser.
6. _*Question*_: What is the JSON response you get back?
7. Modify the get_hello handler to also return the current epoch (unix) time in the same JSON response with the key `timestamp`.
   - _Hint_: you will want to add from time import time to the top of your app.py file. To get the current time, you can use this import like so: int(time()). More info on unix epoch time: [https://en.wikipedia.org/wiki/Unix_time](https://en.wikipedia.org/wiki/Unix_time)
8. _*Question*_: What does the server respond with after doing step B.7?

## C: Creating a client system to interact with your VAS

In this step, you will be creating a VAS client from scratch that will communicate with your VAS from step 2.

1. Outside of the folder you did step A, create a new folder called `vas-client`.
2. In gitlab, Create a new project called `SYSE6301-VAS-Client` in your Gitlab account and give access to the instructor and grader through the Gitlab Dashboard menu (Settings > Members)
3. Go into the new `vas-client` folder and run the command `git init`.
4. On the Gitlab dashboard inside your newly created project, look for the blue `clone` button. Clicking on the blue “clone” button, copy the URL provided underneath the “Clone with HTTPS” (it will look like [https://gitlab.com/your-gitlab-username/SYSE6301-VAS-Client.git](https://gitlab.com/your-gitlab-username/SYSE6301-VAS-Client.git)).
5. In the same terminal/folder you ran the `git init` command in step C.3, run the command: `git remote add origin [URL-you-copied-from-step-3.D]`. This will connect your local git repository to Gitlab.
6. Create an empty file called `readme.md` in the folder.
7. In that same terminal, run `git add .`
8. Then run, `git commit -m "my initial commit"`
9. Then run, `git push origin master`
10. You should now see your files on your Gitlab dashboard. At any point, you can push your work to your Gitlab repository by repeating steps C.7-C.9.
11. In your newly created `vas-client` folder, create a virtual environment: `virtualenv venv` and activate it: `source venv/bin/activate` (again, Windows can be slightly different to activate).
12. Install `requests`: `pip install requests`. This is your HTTP client that will interact with your VAS. Make sure you virtualenv is activated before doing this!
13. Create a file called `client.py`, and put this code:

```python
import requests
import time

# If you are running your VAS on a different port, replace the 5000 with the appropriate port number
base_url = "http://localhost:5000"

vas_response = requests.get(base_url + "/hello/vas")
data = vas_response.json()

print("Time is " + str(data["timestamp"]))

human_time = time.strftime("%a, %d %b %Y %H:%M:%S %Z",
                           time.localtime(data["timestamp"]))
print("Human readable time is " + human_time)

```

14. Make sure that your VAS is running (refer to step B.4) in a different terminal.
15. Inside your virtual environment, run `python client.py`.
16. _*Question*_: What does each line do in this code?

## D: Tying your VAS to Iotery

This step aims to tie your VAS to Iotery, which will allow your systems to interact together. We will be adding Iotery functionality to your VAS in order to create a command form your client (that you did in step C) for a device (like a vehicle or traffic light!) you will create.

1. In your VAS project folder and inside that respective virtualenv, run `pip install iotery-python-server-sdk`. This is a Iotery client wrapper for the Iotery REST API (that simply wraps `requests`, like you just did in step C with your VAS.)
2. In `app.py`, add the iotery server dependency at the top of the file:

```
from iotery_python_server_sdk import Iotery
```

a few lines below the last `import` statement, add `iotery = Iotery("my-key")` where `my-key` is the `API Secret` found at [https://iotery.io/system](https://iotery.io/system).

3. Create a new `POST` route handler (similar to `create_number` that exists in the template) with the route signature of `/devices/<device_uuid>/command-types/<command_type_uuid>`. Call this handler `create_command`. As you can see from the route signature, have this route handler take two parameters: `device_uuid` and `command_type_uuid`. For now, just have the handler return the `command_type_uuid` and `device_uuid` in JSON form. These two uuids will be the Iotery device and command types you will create in the next step.

4. Like you did in the previous assignment, create a new Device Type (`VAS_CLIENT_DEVICE`) and device (`VAS_DEVICE`) and Command Type (`CLIENT_DEVICE_COMMAND`) on the Iotery Dashboard. We are now seeing 3 systems take shape: VAS+Iotery (2 systems composed as one), a client (`client.app`), and a device (we will create that in step E).

5. In your VAS in the `create_command` handler, have your VAS take the given `command_type_uuid` and `device_uuid` and create a command for the device you just created. Do this by placing these these two lines of code in your `create_command` handler:

```python
res = iotery.createDeviceCommandInstance(deviceUuid=device_uuid, data={"commandTypeUuid": command_type_uuid})
return res.json()
```

6. Make sure your server still runs without build errors: `python app.py`. We will exercise this route after the next section when we tie all the systems together.

## E: Creating your device

We will be implementing the final system: the device. This "device" is analogous to the vehicle, traffic light, or other system in the last IoT assignment, as well as the upcoming larger project. For now, we will keep it simple. We will be utilizing your `vas-client` project to create the device (for simplicity).

1. In your `vas-client` folder, create a python file called `device.py`. This will emulate your device.
2. In your `vas-client` folder and inside that respective virtualenv, run `pip install iotery-embedded-python-sdk`. This should be familiar - it is very similar to what you did in the previous assignment.
3. In the `device.py` file, place this code:

```python

from iotery_embedded_python_sdk import Iotery

device_serial = "VAS_DEVICE"
device_key = "VAS_DEVICE"
device_secret = "VAS_DEVICE"
# Get your team ID from https://iotery.io/system
team_id = "my-team-id"

device_connector = Iotery()
device_details = device_connector.getDeviceTokenBasic(
    data={"key": device_key, "serial": device_serial, "secret": device_secret, "teamUuid": team_id})
device_connector.set_token(device_details["token"])

command_instances = device_connector.getDeviceUnexecutedCommandInstanceList(
    deviceUuid=device_details["device"]["uuid"])

for command_instance in command_instances["commands"]:
    print(command_instance)
```

replacing the appropriate serial, key, and secret and team ID with what you did when you created your device in step D.4.

4. Run your `device` and make sure that it does not produce any errors (always making sure your virtual env is active): `python device.py`.
5. _*Question*_: What does each line of this file you created do?
6. Write some code to in the provided `for` loop that has the device set the command as executed.
   - Hint: you will want to use the SDK method `device_connector.setCommandInstanceAsExecuted(commandInstanceUuid=command_instance["uuid"], data={"timestamp": int(time())})`
   - Another hint: remember, `time` function needs to be imported: `from time import time`!

## F: Tying the Systems Together

This is the final step to tie all the systems together. We must add functionality to the client to tell the VAS to create the command for the device.

1. In `client.py` of your `vas-client` project folder, add code at the bottom to give the order to your VAS to create the command:

```
requests.post(base_url + "/devices/" + device_uuid + "/command-types/" + command_uuid)
```

Remember, you will need to specify the `device_uuid` and `command_uuid` for the devices your created in step D.4. You can get the `uuid` from the dashboard.

2.  In `device.py`, print the string `"Command Executed!"` (`print("Command Executed")`) when the device executes the command (you wrote the code to do this in step E.6).

3.  Time to tie it all the systems together and make them work in concert! Make sure that your VAS is running inside your virtual environment: `python app.py`.

4.  Create the command by running `python client.py` in your `vas-client`.

5.  In a separate terminal (but still with a virtual env activated), have your device execute the command by running `python device.py`.

6.  You should see an HTTP request come in from on your VAS from your `client.py`, and your device print `Command Executed!`.

7.  _*Question*_: Sketch a diagram in your favorite diagramming software (Powerpoint, Word, MS Paint, etc) of the systems, labeling each one, and indicating with arrows and annotations how they work together. Save the image and include it in your `vas-client` folder.

8.  We added some dependencies to your VAS (namely, the Iotery SDK). We need to make sure to include those in the requirements file so that heroku knows to install it. In your activated virtualenv in your VAS project folder, run: `pip freeze > requirements.txt`.

9.  Push all your code to the gitlab repos you made:

    - In your VAS project folder: `git add .`, `git commit -m "final"`, `git push origin master`.
    - In your `vas-client` folder: `git add .`, `git commit -m "final"`, `git push origin master`.

10. _*Question*_: Get creative and give 3 different real-world system examples that mirror the example problem you just completed. Make sure you describe each of the real-world systems and how they are similar to the example (`VAS_DEVICE`).

11. _*Question*_: Why do we have the client create a command through the VAS instead of having the client create it directly with Iotery? Provide at least 1 reason.

12. _*Question*_: Explain why we think of the VAS and Iotery as a composed system.

# Bonus and Project Starters

If you plan on coding a lot in the project, then the following steps are necessary to follow to prepare you to complete the project. It is highly recommended you do the following steps.

## G: To the cloud! Deploying your VAS

Up until this point, you have developed locally on your systems. In order to interact consistently with Iotery and other VAS, you must put up your VAS to the cloud so it can be publically accessible. We do this with Heroku.

1. Sign up for a heroku account if you have not already. See [https://gitlab.com/systems-iot/api-server-implementation](https://gitlab.com/systems-iot/api-server-implementation) for walkthrough. Make sure to also install the `heroku cli`: [https://devcenter.heroku.com/articles/heroku-cli](https://devcenter.heroku.com/articles/heroku-cli)
   - You will want to login via the CLI inside your VAS project folder: `heroku login`. Follow the directions to login.
2. Visit the Heroku dashboard at [https://dashboard.heroku.com/apps](https://dashboard.heroku.com/apps) and look for the "new" button. Choose the `Create new app` option. Select an appropriate app name that is not already taken (such as syse6301-vas-yourname-au2019).
3. Once created, you need to hook your VAS up to be automatically deployed by Heroku (we use heroku's git for this)...the directions are also on your app dashboard in heroku under "Existing git repository":
   - `heroku git:remote -a your-app-name`
   - `git add .`
   - `git commit -am "deploy"`
   - `git push heroku master`
4. After heroku finishes deployment, should be able to see your deployment work at the URL that is provided in the console after deployment is finished. It will look like: `https://syse6301-vas-yurk-au2019.herokuapp.com/`. Verify the server is running by visting [https://syse6301-vas-yurk-au2019.herokuapp.com/hello/vas](https://syse6301-vas-yurk-au2019.herokuapp.com/hello/vas) in a browser. Notice we only changed `http://localhost:5000` to `https://syse6301-vas-yurk-au2019.herokuapp.com`.
   - If you see the JSON response from earlier, it worked! Your VAS is now deployed to the web for all to see! Proof: Check it out from a smartphone browser or have a friend look at it from their computer.
   - If you get an error message in your browser, review the provided video on how to see the logs of Heroku and debug problems.

## H: Notifying the VAS when Something Happens in Iotery

Up until this point, the client is the only system that can take action on something (and it is driven by a human). How do we take action when a device executes a command? To do this, we use Iotery `Webhooks`.

1. Iotery, create a webhook: Go to the Iotery Dashboard: [https://iotery.io/webhooks](https://iotery.io/webhooks)
2. Create a webhook with the following information:
   - webhook action: `Set Command Instance As Executed`
   - webhook method: `POST`
   - webhook address: `https://syse6301-vas-yourname-au2019.herokuapp.com/command-instances/executed` (replace the base URL with your deployed heroku URL from step G.4)
   - Press the save button
3. In your VAS, we need to create a handler that takes care of the Iotery Webhook that is fired when a device executes a command instance. To do this, add a route handler in your `app.py` of your VAS project:

```python
@app.route("/command-instances/executed", methods=["POST"])
def notify_of_executed_command():
    body = request.json
    print("Command executed at " + str(body["in"]["timestamp"]))
    return {}
```

4. Redeploy to Heroku:

   - `git add .`
   - `git commit -am "deploy"`
   - `git push heroku master`

5. To verify it worked, in the same directory as where you run your VAS, run the command: `heroku logs --tail`. This will give you a printout of the logs of the heroku deployment. Leaving this running (assuming it shows that your server is running with no errors) will show you when a device notifies Iotery of the a command instance being executed.

6. In your `vas-client` folder, change the `base_url` variable to the base URL of your heroku deployment (like `https://syse6301-vas-yourname-au2019.herokuapp.com`).
7. Run `python client.py` to create the command using your deployed VAS, then run `python device.py` to have the device execute the command. If you are watching your heroku logs (`heroku logs --tail`), you should see a log entry that says "`Command executed at 1566591182`". This is Iotery sending a RESTful POST to your VAS notifying it that the device exectued the command!
